﻿////// Copyright (c) Micheal Richardson. All rights reserved


var ColorCard = {};

(function () {
    function initialize() {
        //Add ColorCard header
        var header = document.getElementById("header");
        if (Boolean(header)) {
            var logo = document.createElement("img");
            logo.src = "images/ColorCard-SmallLogo.png";
            logo.alt = "ColorCard";
            logo.id = "smallLogo";
            header.appendChild(logo);
        }

    }

    document.addEventListener("DOMContentLoaded", initialize, false);
})();