﻿////// Copyright (c) Micheal Richardson. All rights reserved

(function () {
    "use strict";

    var isPrintContractRegistered = false;

    var page = WinJS.UI.Pages.define("default.html", {
        ready: function (element, options) {
            registerForPrintContract();
        }
    });

    function registerForPrintContract() {
        var printManager = Windows.Graphics.Printing.PrintManager.getForCurrentView();
            printManager.onprinttaskrequested = onPrintTaskRequested;
            isPrintContractRegistered = true;
           WinJS.log && WinJS.log("Print Contract registered. Use the Charms bar to print.", "sample", "status");
    }

  
    /// Print event handler for printing via the PrintManager API. The user has to manually invoke
    /// the print charm after this function is executed.
    function onPrintTaskRequested(printEvent) {
        var printTask = printEvent.request.createPrintTask("ColorCard", function (args) {
            args.setSource(MSApp.getHtmlPrintDocumentSource(document));

            // Choose the printer options to be shown.
            // The order in which the options are appended determines the order in which they appear in the UI
            printTask.options.displayedOptions.clear();
            printTask.options.displayedOptions.append(Windows.Graphics.Printing.StandardPrintTaskOptions.copies);
            printTask.options.displayedOptions.append(Windows.Graphics.Printing.StandardPrintTaskOptions.mediaSize);
            printTask.options.displayedOptions.append(Windows.Graphics.Printing.StandardPrintTaskOptions.orientation);

            // Preset the default value of the printer option
            printTask.options.mediaSize = Windows.Graphics.Printing.PrintMediaSize.northAmericaLegal;
            printTask.options.orientation = Windows.Graphics.Printing.PrintOrientation.landscape;
            // Register the handler for print task completion event
            printTask.oncompleted = onPrintTaskCompleted;
        });
    }

    /// Print Task event handler is invoked when the print job is completed.
    function onPrintTaskCompleted(printTaskCompletionEvent) {
        // Notify the user about the failure
        if (printTaskCompletionEvent.completion === Windows.Graphics.Printing.PrintTaskCompletion.failed) {
            WinJS.log && WinJS.log("Failed to print.", "ColorCard", "error");
        }
    }
})();

function print(evt) {
    // Optionally, functions to be executed immediately before and after printing can be configured as following:
    //window.document.body.onbeforeprint = beforePrint;
    //window.document.body.onafterprint = afterPrint;

    // If the print contract is registered, the print experience is invoked.
    Windows.Graphics.Printing.PrintManager.showPrintUIAsync();
}

WinJS.UI.eventHandler(print);