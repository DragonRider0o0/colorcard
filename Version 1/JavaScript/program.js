﻿//// Copyright (c) Micheal Richardson. All rights reserved


function showMessage(message, isError) {
    // var statusDiv = document.getElementById("statusMessage");
    //  if (statusDiv)
    // {
    //    statusDiv.innerText = message;
    //    statusDiv.style.color = isError ? "blue" : "green";
    // }
}

function displayStatus(message) {
    showMessage(message, false);
}

function displayError(message) {
    showMessage(message, true);
}

window.onerror = function (msg, url, line) { displayError("Error: " + msg + " url = " + url + " line = " + line); };


// Functions to convert from and to the 32-bit int used to represent color in Windows.UI.Input.Inking.InkManager.

// Convenience function used by color converters.
// Assumes arg num is a number (0..255); we convert it into a 2-digit hex string.

function byteHex(num) {
    var hex = num.toString(16);
    if (hex.length === 1) {
        hex = "0" + hex;
    }
    return hex;
}

// Convert from Windows.UI.Input.Inking's color code to html's color hex string.

function toColorString(color) {
    return "#" + byteHex(color.r) + byteHex(color.g) + byteHex(color.b);
}

// Convert from the few color names used in this app to Windows.UI.Input.Inking's color code.
// If it isn't one of those, then decode the hex string.  Otherwise return gray.
// The alpha component is always set to full (255).
function toColorStruct(color) {
    switch (color) {
        // Ink colors
        case "Black":
            return Windows.UI.Colors.black;
        case "Blue":
            return Windows.UI.Colors.blue;
        case "Red":
            return Windows.UI.Colors.red;
        case "Green":
            return Windows.UI.Colors.green;
        case "Yellow":
            return Windows.UI.Colors.yellow;
        case "Brown":
            return Windows.UI.Colors.brown;
        case "Purple":
            return Windows.UI.Colors.purple;
        case "Orange":
            return Windows.UI.Colors.orange;
        case "Gray":
            return Windows.UI.Colors.gray;
        case "White":
            return Windows.UI.Colors.white;
    }

    if ((color.length === 7) && (color.charAt(0) === "#")) {
        var R = parseInt(color.substr(1, 2), 16);
        var G = parseInt(color.substr(3, 2), 16);
        var B = parseInt(color.substr(5, 2), 16);
        return Windows.UI.ColorHelper.fromArgb(255, R, G, B);
    }

    return Windows.UI.Colors.gray;
}

// Global variable representing the application.
var app;

// Global variables representing the ink interface.
// The usage of a global variable for drawingAttributes is not completely necessary,
// just a convenience.  One could always re-fetch the current drawingAttributes
// from the inkManager.
var inkManager = new Windows.UI.Input.Inking.InkManager();
var drawingAttributes = new Windows.UI.Input.Inking.InkDrawingAttributes();
drawingAttributes.fitToCurve = true;
inkManager.setDefaultDrawingAttributes(drawingAttributes);

// These are the global canvases (and their 2D contexts) for Background, for drawing ink,
// and erasing.
var backgroundCanvas;
var backgroundContext;
var inkCanvas;
var inkContext;
var brushList;

// The "mode" of whether we are Background, inking, lassoing, or erasing is controlled by this global variable,
// which should be pointing to either backgroundContext, inkContext, or selContext.
// In lassoing mode (when context points to selContext), we might also be in erasing mode;
// the state of lassoing vs. erasing is kept inside the ink manager, in attribute "mode", which will
// have a value from enum Windows.UI.Input.Inking.InkManipulationMode, one of either "selecting"
// or "erasing" (the other value being "inking" but in that case context will be pointing to one of the other
// 2 canvases).
var context;

// Three functions to save and restore the current mode, and to clear this state.

// Note that we can get into erasing mode in one of two ways: there is a eraser button in the toolbar,
// and some pens have an active back end that is meant to represent erasing.  If we get into erasing
// mode via the button, we stay in that mode until another button is pushed.  If we get into erasing
// mode via the eraser end of the stylus, we should switch out of it when the user switches to the ink
// end of the stylus.  And we want to return to the mode we were in before this happened.  Thus we
// maintain a shallow stack (depth 1) of "mode" info.

var savedContext = null;
var savedStyle = null;
var savedCursor = null;
var savedMode = null; 
var animationActive = false;

function clearMode() {
    savedContext = null;
    savedStyle = null;
    savedCursor = null;
    savedMode = null;
}

function saveMode() {
    if (!savedContext) {
        savedStyle = context.strokeStyle;
        savedContext = context;
        savedCursor = inkCanvas.style.cursor;
        savedMode = inkManager.mode;
    }
}

function restoreMode() {
    if (savedContext) {
        context = savedContext;
        context.strokeStyle = savedStyle;
        inkManager.mode = savedMode;
        inkCanvas.style.cursor = savedCursor;
        clearMode();
    }
}

// Global variable representing the pattern used when in select mode.  This is an 8*1 image with 4 bits set,
// then 4 bits cleared, to give us a dashed line when drawing a lasso.
//var selPattern;

// Global variable representing the application toolbar at the bottom of the screen.
var appbar;



// Global pointers to flyouts invoked by the appbar.
//var inkColorsFlyout;
//var inkWidthsFlyout;

//Returns true if this stroke is a Background stroke.
function isBackground(stroke) {
    var att = stroke.drawingAttributes;
    return att.color.a < 200;
}

// Makes all strokes a part of the selection.
function selectAll() {
    inkManager.getStrokes().forEach(function (stroke) {
        stroke.selected = true;
    });
}



// Note that we cannot just set the width in stroke.drawingAttributes.size.width,
// or the color in stroke.drawingAttributes.color.
// The stroke API supports get and put operations for drawingAttributes,
// but we must execute those operations separately, and change any values
// inside drawingAttributes between those operations.

// Change the color and width in the default (used for new strokes) to the values
// currently set in the current context.
function setDefaults() {
    var strokeSize = drawingAttributes.size;
    strokeSize.width = strokeSize.height = 4;
    drawingAttributes.size = strokeSize;
    var color = toColorStruct(context.strokeStyle);
    color.a = (context === backgroundContext) ? 128 : 255;
    drawingAttributes.color = color;
    inkManager.setDefaultDrawingAttributes(drawingAttributes);
}

// Four functions to switch back and forth between ink mode, highlight mode, select mode, and erase mode.
// There is also a temp erase mode, which uses the saveMode()/restoreMode() functions to
// return us to our previous mode when done erasing.  This is used for quick erasers using the back end
// of the pen (for those pens that have that).saveMode()
// NOTE: The erase modes also attempt to set the mouse/pen cursor to the image of a chalkboard eraser
// (stored in images/erase.cur), but as of this writing cursor switching is not working.


function inkMode() {
    clearMode();
    context = inkContext;
    inkManager.mode = Windows.UI.Input.Inking.InkManipulationMode.inking;
    setDefaults();
}

function eraseMode() {
    clearMode();
    inkContext.strokeStyle = "rgba(255,255,255,0.0)";
    context = inkContext;
    inkManager.mode = Windows.UI.Input.Inking.InkManipulationMode.erasing;
    inkCanvas.style.cursor = "url(images/erase.cur), auto";
}

function tempEraseMode() {
    saveMode();
    inkContext.strokeStyle = "rgba(255,255,255,0.0)";
    context = inkContext;
    inkManager.mode = inkManager.mode = Windows.UI.Input.Inking.InkManipulationMode.erasing;
    inkCanvas.style.cursor = "url(images/erase.cur), auto";
}

// Set the width of a stroke.  Return true if we actually changed it.
// Note that we cannot just set the width in stroke.drawingAttributes.size.width.
// The stroke API supports get and put operations for drawingAttributes,
// but we must execute those operations separately, and change any values
// inside drawingAttributes between those operations.
function shapeStroke(stroke, width) {
    var att = stroke.drawingAttributes;
    var strokeSize = att.size;
    if (strokeSize.width !== width) {
        strokeSize.width = strokeSize.height = width;
        att.size = strokeSize;
        stroke.drawingAttributes = att;
        return true;
    }
    else {
        return false;
    }
}

// Set the color (and alpha) of a stroke.  Return true if we actually changed it.
// Note that we cannot just set the color in stroke.drawingAttributes.color.
// The stroke API supports get and put operations for drawingAttributes,
// but we must execute those operations separately, and change any values
// inside drawingAttributes between those operations.
function colorStroke(stroke, color) {
    var att = stroke.drawingAttributes;
    var clr = toColorStruct(color);
    if (att.color !== clr) {
        att.color = clr;
        stroke.drawingAttributes = att;
        return true;
    }
    else {
        return false;
    }
}

// Global memory of the current pointID (for pen, and, separately, for touch).
// We ignore handlePointerMove() and handlePointerUp() calls that don't use the same
// pointID as the most recent handlePointerDown() call.  This is because the user sometimes
// accidentally nudges the mouse while inking or touching.  This can cause move events
// for that mouse that have different x,y coordinates than the ink trace or touch path
// we are currently handling.

// MSPointer* events maintain this pointId so that one can track individual fingers,
// the pen, and the mouse.

// Note that when the pen fails to leave the area where it can be sensed, it does NOT
// get a new ID; so it is possible for 2 or more consecutive strokes to have the same ID.

var penID = -1;


// We will accept pen down or mouse left down as the start of a stroke.
// We will accept touch down or mouse right down as the start of a touch.
function handlePointerDown(evt) {
    try {
        var pt = { x: 0.0, y: 0.0 };
        inkManager.selectWithLine(pt, pt);

        pt = evt.currentPoint;
        evt.currentPoint

        if (pt.properties.isEraser) // the back side of a pen, which we treat as an eraser
        {
            tempEraseMode();
        }
        else {
            restoreMode();
        }

        context.beginPath();
        context.moveTo(pt.position.x, pt.position.y);

        inkManager.processPointerDown(pt);
        penID = evt.pointerId;
    }
    catch (e) {
        displayError("handlePointerDown " + e.toString());
    }
}

function handlePointerMove(evt) {
    try {
        if (evt.pointerId === penID) {
            var pt = evt.currentPoint;
            context.lineTo(pt.position.x, pt.position.y);
            context.stroke();
            // Get all the points we missed and feed them to inkManager.
            // The array pts has the oldest point in position length-1; the most recent point is in position 0.
            // Actually, the point in position 0 is the same as the point in pt above (returned by evt.currentPoint).
            var pts = evt.intermediatePoints;
            for (var i = pts.length - 1; i >= 0 ; i--) {
                inkManager.processPointerUpdate(pts[i]);
            }
        }
    }
    catch (e) {
        displayError("handlePointerMove " + e.toString());
    }
}

function handlePointerUp(evt) {
    try {
        if (evt.pointerId === penID) {
            penID = -1;
            var pt = evt.currentPoint;
            context.lineTo(pt.position.x, pt.position.y);
            context.stroke();
            context.closePath();

            var rect = inkManager.processPointerUp(pt);
            if (inkManager.mode === Windows.UI.Input.Inking.InkManipulationMode.selecting) {
                detachSelection(rect);
            }

            renderAllStrokes();
        }
    }
    catch (e) {
        displayError("handlePointerUp " + e.toString());
    }
}

// We treat the event of the pen leaving the canvas as the same as the pen lifting;
// it completes the stroke.
function handlePointerOut(evt) {
    try {
        if (evt.pointerId === penID) {
            var pt = evt.currentPoint;
            context.lineTo(pt.position.x, pt.position.y);
            context.stroke();
            context.closePath();
            inkManager.processPointerUp(pt);
            penID = -1;
            //touchID =
            renderAllStrokes();
        }
    }
    catch (e) {
        displayError("handlePointerOut " + e.toString());
    }
}

function handleTap(evt) {
    var pt = { x: 0.0, y: 0.0 };
    inkManager.selectWithLine(pt, pt);

    pt = evt.currentPoint;
    evt.currentPoint

    if (pt.properties.isEraser) // the back side of a pen, which we treat as an eraser
    {
        tempEraseMode();
    }
    else {
        restoreMode();
    }

    context.beginPath();
    context.moveTo(pt.position.x, pt.position.y);

    inkManager.processPointerDown(pt);
    penID = evt.pointerId;
}


// Set the order and visibility of icons in the app bar based on view state
function setView() {
    switch (Windows.UI.ViewManagement.ApplicationView.value) {
        case Windows.UI.ViewManagement.ApplicationViewState.filled:
            appbar.showOnlyCommands(["Black", "Green", "Brown", "Red", "Purple", "Blue", "ModeErase", "Save", "Load", "Print"], true);
            break;
        case Windows.UI.ViewManagement.ApplicationViewState.fullScreenLandscape:
            appbar.showCommands(["Black", "Green", "Brown", "Red", "Purple", "Blue", "ModeErase", "Save", "Load", "Print"], true);
            break;
        case Windows.UI.ViewManagement.ApplicationViewState.fullScreenPortrait:
            appbar.showOnlyCommands(["Black", "Green", "Brown", "Red", "Purple", "Blue", "ModeErase", "Save", "Load", "Print"], true);
            break;
        case Windows.UI.ViewManagement.ApplicationViewState.snapped:
            appbar.showOnlyCommands(["Black", "Red", "Green", "Blue", "ModeErase"], true);
            break;
    }
}

//Draws a single stroke into a specified canvas 2D context, with a specified color and width.
function renderStroke(stroke, color, width, ctx) {
    ctx.save();

    try {
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.lineWidth = width;

        var first = true;
        stroke.getRenderingSegments().forEach(function (segment) {
            if (first) {
                ctx.moveTo(segment.position.x, segment.position.y);
                first = false;
            }
            else {
                ctx.bezierCurveTo(segment.bezierControlPoint1.x, segment.bezierControlPoint1.y,
                                  segment.bezierControlPoint2.x, segment.bezierControlPoint2.y,
                                  segment.position.x, segment.position.y);
            }
        });

        ctx.stroke();
        ctx.closePath();

        ctx.restore();
    }
    catch (e) {
        ctx.restore();
        displayError("renderStroke " + e.toString());
    }
}

// This draws the paper onto the highlight canvas, which is the lowest canvas.
function renderPaper() {
    var height = backgroundCanvas.height;
    var bottom = height - 0.5;
    var right = backgroundCanvas.width - 0.5;

    backgroundContext.save();
    // This draws over the background so the page can be printed correctly.
    try {
        backgroundContext.strokeStyle = "#ECECEC";
        backgroundContext.lineWidth = 2000;
        backgroundContext.moveTo(500, 500);
        backgroundContext.lineTo((backgroundCanvas.width), 500);
        backgroundContext.lineTo(backgroundCanvas.width, backgroundCanvas.height);
        backgroundContext.lineTo(500, (backgroundCanvas.height));
        backgroundContext.lineTo(500, 500);

        backgroundContext.stroke();
        // This draws a the small logo onto the highlight canvas, which is the lowest canvas.
        var img = document.getElementById("smallLogo");
        backgroundContext.drawImage(img, 10, 10, 450, 50);

    }
    catch (e) {
        backgroundContext.restore();
        displayError("renderPaper " + e.toString());
    }

    
}

// Redraws (from the beginning) all strokes in the canvases.  All canvases are erased,
// then the paper is drawn, then all the strokes are drawn.
function renderAllStrokes() {
    //selContext.clearRect(0, 0, backCanvas.width, backCanvas.height);
    inkContext.clearRect(0, 0, inkCanvas.width, inkCanvas.height);
    backgroundContext.clearRect(0, 0, backgroundCanvas.width, backgroundCanvas.height);

    renderPaper();

    inkManager.getStrokes().forEach(function (stroke) {
        var att = stroke.drawingAttributes;
        var color = toColorString(att.color);
        var strokeSize = att.size;
        var width = strokeSize.width;
        var bk = isBackground(stroke);
        var ctx = bk ? backgroundContext : inkContext;

        if (stroke.selected) {
            renderStroke(stroke, color, width * 2, ctx);
            var stripe = bk ? "Azure" : "White";
            var w = width - (bk ? 3 : 1);
            renderStroke(stroke, stripe, w, ctx);
        }
        else {
            renderStroke(stroke, color, width, ctx);
        }
    });
}

function clear() {
    try {

        selectAll();
        inkManager.deleteSelected();
        inkMode();

        renderAllStrokes();
        displayStatus("");
        displayError("");
    }
    catch (e) {
        displayError("clear: " + e.toString());
    }
}

// A generic function for use for any async error function (the second arg to a then() method).
function asyncError(e) {
    displayError("Async error: " + e.toString());
}


// A button handler which fetches the ID from the button, which should
// be a color name.  We set the strokeStyle of the inking canvas to this color,
// then set the system into ink mode (which will cause the ink manager
// to change its defaults for new strokes to match the ink canvas).
// If any ink strokes are currently selected,
// we also change their color to this value.  If any strokes are changed
// we must re-render the dirty areas.
function inkColor(evt) {
    //inkColorsFlyout.winControl.hide();
    if (evt.srcElement.id == "Brown")
        inkContext.strokeStyle = "#663300"
    else if (evt.srcElement.id == "Purple")
        inkContext.strokeStyle = "#660066"
    else
        inkContext.strokeStyle = evt.srcElement.id;

    if (evt.srcElement.id == "Green")
        inkCanvas.style.cursor = "url(images/greenmarker.cur), auto";
    else if (evt.srcElement.id == "Brown")
        inkCanvas.style.cursor = "url(images/brownmarker.cur), auto";
    else if (evt.srcElement.id == "Red")
        inkCanvas.style.cursor = "url(images/redmarker.cur), auto";
    else if (evt.srcElement.id == "Purple")
        inkCanvas.style.cursor = "url(images/purplemarker.cur), auto";
    else if (evt.srcElement.id == "Blue")
        inkCanvas.style.cursor = "url(images/bluemarker.cur), auto";
    else
        inkCanvas.style.cursor = "url(images/marker.cur), auto";
    saveMode();
    inkMode();

    var redraw = false;
    inkManager.getStrokes().forEach(function (stroke) {
        if (stroke.selected && !isBackground(stroke)) {
            if (colorStroke(stroke, inkContext.strokeStyle)) {
                redraw = true;
            }
        }
    });
    if (redraw) {
        renderAllStrokes();
    }
}


// A keypress handler which closes the program.
// A normal program should not have this, but it is very
// convenient for testing.
function closeProgram(evt) {
    displayStatus("Closing App ...");
    window.close();
}

// prevent two concurrent loadAsync() operations
var asyncFlag = false;

// Reads a gif file which contains strokes as metadata.
function readInk(storageFile) {
    if (storageFile) {
        // closure variable, visible to all promises in the following chain
        var loadStream = null;
        storageFile.openAsync(Windows.Storage.FileAccessMode.read).then(
            function (stream) {
                // about to call loadAsync()
                // prevent future calls to this API until we are done with the first call
                asyncFlag = true;
                loadStream = stream;
                return inkManager.loadAsync(loadStream); // since we return the promise, it will be executed before the following .done
            }
        ).done(
            function () {
                var strokes = inkManager.getStrokes();
                var c = strokes.length;
                if (c === 0) {
                    displayStatus("File does not contain any ink strokes.");
                }
                else {
                    displayStatus("Loaded " + c + " strokes.");
                    renderAllStrokes();
                }

                // reset asyncFlag, can call loadAsync() once again
                asyncFlag = false;

                // input stream is IClosable interface and requires explicit close
                loadStream.close();
            },
            function (e) {
                displayError("Load failed. Make sure you tried to open a file that can be read by the InkManager.");

                // we still want to reset asyncFlag if an error occurs
                asyncFlag = false;

                // if the error occurred after the stream was opened, close the stream
                if (loadStream) {
                    loadStream.close();
                }
            }
        );
    }
}

// A button handler which fetches the file name via the file picker, then calls readInk() above.
function load(evt) {
    //moreFlyout.winControl.hide();
    if (asyncFlag) {
        return;
    }

    // File pickers don't work in snapped state. Make sure we are not in snapped state, or that we successfully unsnapped
    if (Windows.UI.ViewManagement.ApplicationView.value !== Windows.UI.ViewManagement.ApplicationViewState.snapped || Windows.UI.ViewManagement.ApplicationView.tryUnsnap() === true) {
        // Open the WinRT file picker, set the input folder, and set the input extension.
        var picker = new Windows.Storage.Pickers.FileOpenPicker();
        picker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.documentsLibrary;
        picker.fileTypeFilter.replaceAll([".gif"]);
        picker.pickSingleFileAsync().done(readInk, asyncError);
    }
    else {
        displayError("Cannot unsnap the application. File picker cannot be used in snapped mode.");
    }
}

function writeInk(storageFile) {
    if (storageFile) {
        // closure variable, visible to all promises in the following chain
        var saveStream = null;
        storageFile.openAsync(Windows.Storage.FileAccessMode.readWrite).then(
            function (stream) {
                saveStream = stream;
                return inkManager.saveAsync(saveStream); // since we return the promise, it will be executed before the following .then
            }
        ).done(
            function (result) {
                // print the size of the stream on the screen
                displayStatus("File saved!");

                // output stream is IClosable interface and requires explicit close
                saveStream.close();
            },
            function (e) {
                displayError("Save " + e.toString());

                // if the error occurred after the stream was opened, close the stream                
                if (saveStream) {
                    saveStream.close();
                }
            }
        );
    }
}

// Shows the create file dialog box. Submitting on that form will invoke saveFile() above.
function save(evt) {
    // moreFlyout.winControl.hide();

    // NOTE: make sure that the inkManager has some strokes to save before calling inkManager.saveAsync
    if (inkManager.getStrokes().size > 0) {
        // File pickers don't work in snapped state. Make sure we are not in snapped state, or that we successfully unsnapped
        if (Windows.UI.ViewManagement.ApplicationView.value !== Windows.UI.ViewManagement.ApplicationViewState.snapped || Windows.UI.ViewManagement.ApplicationView.tryUnsnap() === true) {
            var picker = new Windows.Storage.Pickers.FileSavePicker();
            picker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.documentsLibrary;
            //var path = picker.suggestedSaveFile.path;
            //path += "/Color!";
            picker.suggestedFileName = "Page";
            picker.fileTypeChoices.insert("GIF file", [".gif"]);
            picker.defaultFileExtension = ".gif";
            picker.pickSaveFileAsync().done(writeInk, asyncError);


        }
        else {
            displayError("Cannot unsnap the application. File picker cannot be used in snapped mode.");
        }
    }

}

// A keypress handler that only handles a few keys.  This is registered on the entire body.
// Escape will:
//   1. If any dialog boxes are showing, hide them and do nothing else.
//   2. Otherwise, if any strokes are selected, unselect them and do nothing else.
//   3. Otherwise, change to ink mode.
// This sequence allows us to "unpeel the onion" (it is very fast to hit escape 3 times if needed).

// Certain control keys invoke handlers that are otherwise invoked via buttons:
//   ^C  Copy
//   ^V  Paste
//   ^F  Find
//   ^O  Load
//   ^S  Save
//   ^R  Recognize
//   ^Q  Quit (shuts down the Color! app)

// Note that most of these keys have standardized normal uses, and there is system code to handle that
// without our code doing anything.  That code sometimes interferes with our program.  All the functions
// we call from here call evt.preventDefault(), which should stop the default processing, but sometimes we still
// cannot get this code to execute.
function keypress(evt) {
    if (evt.keyCode === 27) // escape
    {
        evt.preventDefault();

        inkMode();
        //}
    }
    else if (evt.keyCode === 14) // control n - New page
    {
        clear(evt);
    }
    else if (evt.keyCode === 15) // control o - Open page
    {
        load(evt);
    }
    else if (evt.keyCode === 19) // control s - Save Page
    {
        save(evt);
    }
    else if (evt.keyCode === 17) // control q - Quit
    {
        closeProgram(evt);
    }
    else if (evt.keyCode === 16) // control p - Print Page
    {
        print(evt);
    }
}

function inkInitialize() {
    // Utility to fetch elements by ID.
    function id(elementId) {
        return document.getElementById(elementId);
    }

    WinJS.UI.processAll().then(
        function () {
            app = WinJS.Application;
            appbar = id("bottomAppBar").winControl;
            appbar.show();

            backgroundCanvas = id("BackgroundCanvas");
            backgroundCanvas.setAttribute("width", backgroundCanvas.offsetWidth);
            backgroundCanvas.setAttribute("height", backgroundCanvas.offsetHeight);
            backgroundContext = backgroundCanvas.getContext("2d");
            backgroundContext.lineWidth = 10;
            backgroundContext.strokeStyle = "Black";
            backgroundContext.lineCap = "round";
            backgroundContext.lineJoin = "round";

            inkCanvas = id("InkCanvas");
            inkCanvas.setAttribute("width", inkCanvas.offsetWidth);
            inkCanvas.setAttribute("height", inkCanvas.offsetHeight);
            inkContext = inkCanvas.getContext("2d");
            inkContext.lineWidth = 4;
            inkContext.strokeStyle = "Black";
            inkContext.lineCap = "round";
            inkContext.lineJoin = "round";
            inkCanvas.gestureObject = new MSGesture();


            // Note that we must set the event listeners on the top-most canvas.

            inkCanvas.addEventListener("MSPointerDown", handlePointerDown, false);
            inkCanvas.addEventListener("MSPointerMove", handlePointerMove, false);
            inkCanvas.addEventListener("MSPointerUp", handlePointerUp, false);
            inkCanvas.addEventListener("MSPointerOut", handlePointerOut, false);


            brushList = new Array();
            //selCanvas.addEventListener("MSGestureTap", handleTap, false);

            window.addEventListener("resize", setView);

            setView();


            document.body.addEventListener("keypress", keypress, false);


            //displayStatus("Verba volant, Scripta manet");

            inkCanvas.style.cursor = "url(images/marker.cur), auto";
            inkMode();
            renderPaper();
        }
    ).done(
        function () {
        },
        function (e) {
            displayError("inkInitialize " + e.toString());
        }
    );
}


// Tag the event handlers of the AppBar so that they can be used in a declarative context.
// For security reasons WinJS.UI.processAll and WinJS.Binding.processAll (and related) functions allow only
// functions that are marked as being usable declaratively to be invoked through declarative processing.
WinJS.UI.eventHandler(eraseMode);
WinJS.UI.eventHandler(clear);
WinJS.UI.eventHandler(inkColor);
WinJS.UI.eventHandler(load);
WinJS.UI.eventHandler(save);

document.addEventListener("DOMContentLoaded", inkInitialize, false);
